<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */


function franco_install_tasks()
{

    $tasks = array(
    
        'franco_welcome' => array(
            'display_name' => st( 'Welcome' ),
            'type' => 'normal'
        )
    
    );

}

function franco_welcome() {
 
  drupal_set_message( st( 'Welcome to FrancoMedia\'s Drupal Distribution' ) );

  return st('We are going to walk you through the remaining steps required to set up Drune on your server.'); 
  
}